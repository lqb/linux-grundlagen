# Linux Grundlagen - Konsole

## Hilfe
| Kommando             | Beschreibung                                          | Beispiel              |
| -------------------- | ----------------------------------------------------- | --------------------- |
| [man](#man)          | Manual Pages/System-Referenzhandbücher                | ```man man```         |
| [whatis](#whatis)    | Durchsucht die Indexdatenbank nach Kurzbeschreibungen | ```whatis whatis```   |
| [apropos](#apropos)  | Suche in Manpages und Beschreibunngen                 | ```apropos apropos``` |

## Dateiverwaltung
| Kommando        | Bschreibung
| --------------- | ------------------------------------------------------------- |
| [pwd](#pwd)     | Den Namen des aktuellen Arbeitsverzeichnisses ausgeben        |
| [ls](#ls)       | Verzeichnisinhalte auflisten                                  |
| [mkdir](#mkdir) | Verzeichnisse erstellen                                       |
| [cd](#cd)       | Verzeichnis wechseln                                          |
| [cp](#cp)       | Dateien und Verzeichnisse kopieren                            |
| [mv](#mv)       | Dateien verschieben oder umbenennen                           |
| [touch](#touch) | Zeitstempel von Dateien ändern                                |
| [rm](#rm)       | Dateien oder Verzeichnisse entfernen                          |
| [rmdir](#rmdir) | Leere Verzeichnisse entfernen                                 |
| [echo](#echo)   | Eine Zeile Text anzeigen                                      |
| [cat](#cat)     | Dateien aneinanderhängen und in die Standardausgabe schreiben |

### man ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/man.1.html
```
$ man man
```

--------------------------------------------------------------------------------

### whatis ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/whatis.1.html
```
$ man whatis
```

--------------------------------------------------------------------------------

### apropos ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/apropos.1.html
```
$ man apropos
```

--------------------------------------------------------------------------------

### pwd ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/pwd.1.html
```
$ man pwd
```
#### Beispiel ####
```
$ pwd
/home/user
```
--------------------------------------------------------------------------------

### ls ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/ls.1.html
```
$ man ls
```
#### Beispiel ####
```
$ ls -la
total 40
drwxr-xr-x 5 user user 4096 Jan 30 16:30 .
drwxr-xr-x 3 root root 4096 Dec 13 12:39 ..
-rw------- 1 user user 1370 Jan 29 02:00 .bash_history
-rw-r--r-- 1 user user  220 Dec 13 12:39 .bash_logout
-rw-r--r-- 1 user user 3771 Dec 13 12:39 .bashrc
drwxr-xr-x 2 user user 4096 Dec 13 12:39 .landscape
-rw------- 1 user user   32 Jan 30 16:00 .lesshst
-rw-r--r-- 1 user user    0 Jan 30 15:13 .motd_shown
-rw-r--r-- 1 user user  807 Dec 13 12:39 .profile
drwx------ 2 user user 4096 Dec 21 00:53 .ssh
-rw-r--r-- 1 user user    0 Dec 18 17:00 .sudo_as_admin_successful
-rw-r--r-- 1 user user    0 Jan 30 16:30 a.txt
-rw-r--r-- 1 user user    0 Jan 30 16:30 b.txt
-rw-r--r-- 1 user user    0 Jan 30 16:30 c.txt
drwxr-xr-x 2 user user 4096 Jan 30 16:19 dir1
```
--------------------------------------------------------------------------------

### mkdir ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/mkdir.1.html
```
$ man mkdir
```
#### Beispiel ####
```
$ mkdir dir1
$ ls 
dir1
```
--------------------------------------------------------------------------------

### cd ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/cd.1p.html
#### Beispiel ####
```
$ cd dir1
$ pwd
/home/user/dir1
$ cd ..
$ pwd
/home/user
$ cd /home/user/dir1
$ pwd
/home/user/dir1
```

--------------------------------------------------------------------------------

### cp ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/cp.1.html
```
$ man cp
```
#### Beispiel ####
```
$ touch a.txt
$ ls
a.txt
$ cp a.txt b.txt
$ ls
a.txt  b.txt
```

--------------------------------------------------------------------------------

### mv ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/mv.1.html
```
man mv
```
#### Beispiel ####
```
$ touch a.txt
$ ls
a.txt
$ mv a.txt c.txt
$ ls
c.txt
```

--------------------------------------------------------------------------------

### touch ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/touch.1.html
```
man touch
```
#### Beispiel ####
```
$ ls -l datei1
ls: cannot access 'datei1': No such file or directory
$ touch datei1
$ ls -l datei1
-rw-r--r-- 1 user user 0 Jan 30 16:24 datei1
$ sleep 120
$ ls -l datei1
-rw-r--r-- 1 user user 0 Jan 30 16:24 datei1
$ touch datei1
$ ls -l datei1
-rw-r--r-- 1 randoom randoom 0 Jan 30 16:27 datei1
```
--------------------------------------------------------------------------------

### rm ###
#### Beschreibung ####
https://www.man7.org/linux/man-pages/man1/rm.1.html
```
man rm
```
#### Beispiel ####
```
$ rm a.txt
```

--------------------------------------------------------------------------------

### rmdir ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man2/rmdir.2.html
```
man rmdir
```
#### Beispiel ####
```
$ rmdir dir1
```

--------------------------------------------------------------------------------

### echo ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/echo.1.html
```
man echo
```
#### Beispiel ####
```
$ echo "Hallo Welt"
Hallo Welt
```

--------------------------------------------------------------------------------

### cat ###
#### Beschreibung ####
https://man7.org/linux/man-pages/man1/cat.1.html
```
man cat 
```
#### Beispiel ####
```
$ cat /etc/lsb-release
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.04
DISTRIB_CODENAME=focal
DISTRIB_DESCRIPTION="Ubuntu 20.04.1 LTS"
```

