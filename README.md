# Linux Grundlagen

1. [Konsole](konsole.md)



Lektüre:
- [Linux - Das umfassende Handbuch; ISBN 978-3-8362-1822-1](http://openbook.rheinwerk-verlag.de/linux/index.html)
- [LinuxWiki](https://linuxwiki.de/LinuxGrundlagen)
- [Ubuntuusers - Shell Einführung](https://wiki.ubuntuusers.de/Shell/Einf%C3%BChrung/)


